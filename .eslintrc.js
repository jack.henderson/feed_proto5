module.exports = {
  root: true,
	parser: '@typescript-eslint/parser',
	parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
		impliedStrict: true,
	},
	settings: {
		react: {
			pragma: 'h'
		}
	},
  plugins: [
		'@typescript-eslint',
		'react',
  ],
  extends: [
    'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:mocha/recommended',
		'plugin:chai-expect/recommended',
	],
	rules: {
		'semi': 'off',
		'@typescript-eslint/semi': ['error'],

		'quotes': ['error', 'single', { 'allowTemplateLiterals': true }],

		'eqeqeq': 'error',
		'no-else-return': 'error',
		'no-self-compare': 'error',
		'radix': 'error',
		'require-await': 'error',
		'capitalized-comments': 'error',
		'comma-style': ['error', 'last'],
		'comma-dangle': ['error', 'always-multiline'],
		'eol-last': ['error', 'always'],
		'no-trailing-spaces': ["error", { "skipBlankLines": true }],
		'one-var': ['error', 'never'],
		'prefer-arrow-callback': [ 'error', { 'allowUnboundThis': false } ],
		'prefer-destructuring': 'error',

		'@typescript-eslint/no-namespace': 'off',

		'react/jsx-uses-react': 'error',
		'react/jsx-uses-vars': 'error',
		'jsx-quotes': ['error', 'prefer-single'],
		'mocha/no-mocha-arrows': 'off',
	}
};