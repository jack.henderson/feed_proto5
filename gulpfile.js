const path = require('path');
const { spawn } = require('child_process');
const { task, src, dest, series, parallel } = require('gulp');
const browsersync = require('browser-sync').create();
const rimraf = require('rimraf');


///////////////////////////////////////////////////////////////////////////////
// Project setup
///////////////////////////////////////////////////////////////////////////////
const ROOT_DIR = __dirname;
const APP_DIR = path.join(ROOT_DIR, 'application');
const BUILD_DIR = path.join(ROOT_DIR, '.build');
const APP_OUT_DIR = 'application';

const isDev = () => process.env.NODE_ENV === 'development';
const getOutDir = () => BUILD_DIR;
const getAppOutDir = () => path.join(getOutDir(), APP_OUT_DIR);


///////////////////////////////////////////////////////////////////////////////
// Application Docker
///////////////////////////////////////////////////////////////////////////////
const APP_DOCKER_DIR = path.join(APP_DIR, 'docker');
task('build:app:docker', () => 
	src(path.join(APP_DOCKER_DIR, 'dockerfile'))
		.pipe(dest(getAppOutDir()))
);

///////////////////////////////////////////////////////////////////////////////
// Application Server
///////////////////////////////////////////////////////////////////////////////
const APP_SERVER_DIR = path.join(APP_DIR, 'server');
const getAppServerOutDir = () => path.join(getAppOutDir(), 'server');


task('build:app:server:copyFiles', () => 
	src([
		path.join(APP_SERVER_DIR, 'package.json'),
		path.join(ROOT_DIR, 'yarn.lock')
	]).pipe(dest(getAppServerOutDir()))
)

task('build:app:server:typescript', () => 
	npx(ROOT_DIR,'tsc', 
		'--project', APP_SERVER_DIR,
		'--outDir', getAppServerOutDir()
	)
);

task('build:app:server', parallel(
	'build:app:server:copyFiles',
	'build:app:server:typescript',
));


///////////////////////////////////////////////////////////////////////////////
// Application
///////////////////////////////////////////////////////////////////////////////
task('build:app', parallel(
	'build:app:server',
	'build:app:docker',
));


///////////////////////////////////////////////////////////////////////////////
// Build
///////////////////////////////////////////////////////////////////////////////
task('clean', cb => {
	rimraf(`${BUILD_DIR}`, cb);
});

task('build', parallel(
	'build:app'
));

task('clean-build', series('clean', 'build'));


///////////////////////////////////////////////////////////////////////////////
// Develop
///////////////////////////////////////////////////////////////////////////////
const FEED_SERVER_PORT = '8888';
const FEED_SERVER_HOST = 'localhost';

task('dev:init', cb => { 
	process.env.NODE_ENV = 'development';
	console.log('--- development mode ---');
	cb();
});

task('dev:app:server', () => 
	npxEnv(ROOT_DIR, { FEED_SERVER_PORT, FEED_SERVER_HOST }, 'ts-node-dev',
	path.join(APP_SERVER_DIR, 'src', 'main.ts'),
	'--notify',
));

task('dev:app:browsersync', () => {
	// Short timeout to give the dev server time to come up.
	setTimeout(() => {
		browsersync.init({
			proxy: `${FEED_SERVER_HOST}:${FEED_SERVER_PORT}`
		});
	}, 1000);
})

task('default', series(
	'dev:init',
	parallel(
		'dev:app:server',
		'dev:app:browsersync',
	),
));


///////////////////////////////////////////////////////////////////////////////
// Utils
///////////////////////////////////////////////////////////////////////////////
const npxEnv = (cwd, env, ...cmd) => spawn('npx', cmd, {
	cwd,
	stdio: 'inherit',
	env: {
		...process.env,
		...env
	}
});

const npx = (cwd, ...cmd) => npxEnv(cwd, {}, ...cmd);