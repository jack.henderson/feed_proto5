export type ChannelType = 'rss';

export interface Channel {
	/** Type of channel this represents. */
	type: ChannelType;

	/** The name of the channel. */
	title: string;

	/** The URL to the HTML website corresponding to the channel. */
	link: string;

	/** Phrase or sentence describing the channel. */
	description: string;
}
