export interface Item {
	/** The title of the item. */
	title: string;

	/** The item synopsis. */
	description: string;
}
