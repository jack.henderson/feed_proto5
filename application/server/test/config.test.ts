import 'mocha';
import { expect } from 'chai';

import { makeServerConfig, defaultServerConfig, makeLoggerConfig } from '../src/init';

describe('server : config', () => {
	describe('makeConfig()', () => {
		
		it('returns default config for empty environment object', () => {
			expect(makeServerConfig({}))
			.to.deep.eq(defaultServerConfig);
		});
		
		it('returns config set from environment object', () => {
			const environment = {
				FEED_SERVER_PORT: '1234',
				FEED_SERVER_HOST: '0.0.0.0',
				FEED_SERVER_LOGGING: '/logging/directory',
			};
			
			if(Object.keys(environment).length !== Object.keys(defaultServerConfig).length) {
				throw new Error('Test appears to be invalid: expected `environment` to contain all configuration options.');
			}
			
			const config =  makeServerConfig(environment);
			
			expect(config)
			.to.deep.eq({
				port: 1234,
				host: '0.0.0.0',
				logging: '/logging/directory',
			});
		});
		
		it('returns combined config from partial environment object', () => {
			const environment = {
				FEED_SERVER_HOST: '0.0.0.0',
				FEED_SERVER_LOGGING: '/logging/directory',
			};
			
			if(Object.keys(environment).length === Object.keys(defaultServerConfig).length) {
				throw new Error('Test appears to be invalid: expected `environment` to contain all configuration options.');
			}
			
			expect(makeServerConfig(environment))
			.to.deep.eq({
				port: defaultServerConfig.port,
				host: environment.FEED_SERVER_HOST,
				logging: environment.FEED_SERVER_LOGGING,
			});
			
		});
		
		it('throws for invalid port env var', () => {
			const environment = {
				FEED_SERVER_PORT: 'this is not a valid port',
			};
			
			expect(() => makeServerConfig(environment))
			.to.throw('Invalid FEED_SERVER_PORT: ' + 'this is not a valid port');
		});
	});
	
	describe('makeLoggerConfig()', () => {
		it('returns level for `stdout`', () => {
			const config = {
				...defaultServerConfig,
				logging: 'stdout',
			};

			expect(makeLoggerConfig(config))
				.to.deep.eq({
					level: 'info',
				});
		});

		it('returns file for other value', () => {
			const config = {
				...defaultServerConfig,
				logging: 'some/file/path',
			};

			expect(makeLoggerConfig(config))
				.to.deep.eq({
					file: 'some/file/path',
				});
		});
	});
});
