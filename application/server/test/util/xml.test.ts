import 'mocha';
import { expect } from 'chai';

import { XML } from '../../src/util';


describe('server : util : XML', () => {
	describe('hasCharContent()', () => {
		it('returns false for null', () => {
			expect(XML.hasCharContent(null))
				.to.be.false;
		});

		it('returns false for non CharContent object', () => {
			expect(XML.hasCharContent({ key: 'value' }))
				.to.be.false;
		});

		it('returns false for object with non-string `_` value', () => {
			expect(XML.hasCharContent({ key: 44 }))
				.to.be.false;
		});

		it('returns true for CharContent object', () => {
			expect(XML.hasCharContent({ _: 'content' }))
				.to.be.true;
		});
	});

	describe('charContent', () => {
		it('returns empty string for empty object', () => {
			expect(XML.charContent({}, 'tag'))
				.to.be.empty;
		});

		it('returns empty string for missing tag', () => {
			expect(XML.charContent({ tag: { _: 'bla' }}, 'missing'))
				.to.be.empty;
		});

		it('returns empty string for non char content tag', () => {
			expect(XML.charContent({ tag: 55 }, 'tag'))
				.to.be.empty;
		});

		it('returns char content when present', () => {
			const content = 'the char content';
			expect(XML.charContent({ tag: { _: content }}, 'tag'))
				.to.eq(content);
		});
	});

	describe('parse()', () => {
		it('returns object for valid XML', async () => {
			expect(await XML.parse('<?xml version="1.0" encoding="UTF-8"?><tag attr="value">content<child></child></tag>'))
			.to.deep.eq({
				tag: {
					$: {
						attr: 'value',
					},
					_: 'content',
					child: '',
				},
			});
		});
		
		it('returns null for invalid XML', async () => {
			expect(await XML.parse('<>>,thisIS not valid XML!!!!'))
			.to.be.null;
		});
		
		it('converts tag names to lowercase', async () => {
			expect(await XML.parse('<?xml version="1.0" encoding="UTF-8"?><TAG></TAG>'))
			.to.deep.eq({ tag: '' });
		});
		
		it('converts attr names to lowercase', async () => {
			expect(await XML.parse('<?xml version="1.0" encoding="UTF-8"?><tag ATTR="value"></tag>'))
			.to.deep.eq({
				tag: {
					$: {
						attr: 'value',
					},
				},
			});
		});
	});
});
