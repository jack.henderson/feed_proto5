import 'mocha';
import { expect } from 'chai';

import { getContentTypeFromHeader } from '../../src/util';


describe('server : provider', () => {
	describe('getContentTypeFromHeader()', () => {
		it('returns only the ContentType for headers containing `;`', () => {
			expect(getContentTypeFromHeader('text/html; charset=UTF-8'))
				.to.eq('text/html');
		});

		it('returns the ContentType unaltered for simple headers', () => {
			expect(getContentTypeFromHeader('text/html'))
				.to.eq('text/html');
		});
	});
});
