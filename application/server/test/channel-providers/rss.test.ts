import 'mocha';
import { expect, assert } from 'chai';

import { RSS } from '../../src/channel-provider/providers/rss';
import { ChannelProviderSource } from '../../src/channel-provider/source';
import { ChannelProviderResult, ProvideChannelResult } from '../../src/channel-provider/result';
import { Channel } from '../../../common/src';
import { exampleRSSContent } from './examples/rss-examples';


describe('server : provider : rss', () => {

	describe('provider()', () => {
		describe('valid feed', () => {
			const source: ChannelProviderSource = {
				contentType: 'application/xml',
				content: exampleRSSContent,
			};

			let result: ChannelProviderResult;
			before(async () => {
				try {
					result = await RSS.provider(source);
				} catch(err) {
					assert.fail(err);
				}
			});

			it('returns OK result for valid RSS feed', async () => {
				const result: ChannelProviderResult = await RSS.provider(source);
				expect(result.status)
					.to.eq('ok');
			});

			it('provides expected channel details', () => {
				if(!ProvideChannelResult.isOk(result)) {
					assert.fail('result is not OK.');
				}

				expect(result.channel)
					.to.deep.eq({
						title: 'FeedForAll Sample Feed',
						link: 'http://www.feedforall.com/industry-solutions.htm',
						description: 'RSS is a fascinating technology. The uses for RSS are expanding daily. Take a closer look at how various industries are using the benefits of RSS in their businesses.',
						type: 'rss',
					});
			});

			it('provides expected number of Items', () => {
				if(!ProvideChannelResult.isOk(result)) {
					assert.fail('result is not OK.');
				}

				expect(result.items.length)
					.to.eq(9);
			});

			it('provides expected Item details', () => {
				if(!ProvideChannelResult.isOk(result)) {
					assert.fail('result is not OK.');
				}

				expect(result.items[0])
					.to.deep.eq({
						title: 'RSS Solutions for Restaurants',
						description: `<b>FeedForAll </b>helps Restaurant's communicate with customers. Let your customers know the latest specials or events.<br>\n<br>\nRSS feed uses include:<br>\n<i><font color="#FF0000">Daily Specials <br>\nEntertainment <br>\nCalendar of Events </i></font>`,
					});
			});
		});
	});
	describe('acceptContentType()', () => {
		it('accepts application/rss+xml', () => {
			expect(RSS.acceptContentType('application/rss+xml'))
				.to.be.true;
		});

		it('accepts application/rdf+xml', () => {
			expect(RSS.acceptContentType('application/rdf+xml'))
				.to.be.true;
		});

		it('accepts application/atom+xml', () => {
			expect(RSS.acceptContentType('application/atom+xml'))
				.to.be.true;
		});

		it('accepts application/xml', () => {
			expect(RSS.acceptContentType('application/xml'))
				.to.be.true;
		});

		it('accepts text/xml', () => {
			expect(RSS.acceptContentType('text/xml'))
				.to.be.true;
		});

		it('rejects text/css', () => {
			expect(RSS.acceptContentType('text/css'))
				.to.be.false;
		});

		it('rejects text/html', () => {
			expect(RSS.acceptContentType('text/html'))
				.to.be.false;
		});

		it('rejects text/javascript', () => {
			expect(RSS.acceptContentType('text/javascript'))
				.to.be.false;
		});

		it('rejects application/json', () => {
			expect(RSS.acceptContentType('application/json'))
				.to.be.false;
		});
	});

	describe('parseRSS()', () => {
		it('returns null for non-xml content', async () => {
			expect(await RSS.parseRSS('thisIS__NOT<validXML!'))
				.to.be.null;
		});

		it('returns null for non-RSS XML', async () => {
			expect(await RSS.parseRSS('<?xml version="1.0" encoding="UTF-8"?><tag attr="value">content<child></child></tag>'))
				.to.be.null;
		});

		it('returns RSS object for RSS XML', async () => {
			expect(await RSS.parseRSS(minimalExampleRSS))
				.to.deep.eq(minimalExampleFeed);
		});
	});

	describe('makeChannel()', () => {
		let channel: Channel;

		beforeEach(() => {
			channel = RSS.makeChannel(minimalExampleFeed);
		});

		it('gets valid title', () => {
			expect(channel.title)
				.to.eq('bla');
		});

		it('gets valid link', () => {
			expect(channel.link)
				.to.eq('example.com');
		});

		it('gets valid descriptions', () => {
			expect(channel.description)
				.to.eq('blabla');
		});
	});

	describe('makeItems()', () => {
		it('returns [] for empty items', () => {
			const feed = {
				channel: {
					title: { _: 'empty feed' },
					link: { _: 'dev/null' },
					description: { _: 'no items here' },
					item: [],
				},
			};
			expect(RSS.makeItems(feed))
				.to.be.empty;
		});


		it('returns correct item info', () => {
			expect(RSS.makeItems(minimalExampleFeed))
				.to.deep.eq([
					{
						title: 'first item',
						description: 'first description',
					},
					{
						title: 'second item',
						description: 'second description',
					},
				]);
		});
	});
});

const minimalExampleRSS = `<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>bla</title>
		<link>example.com</link>
		<description>blabla</description>
		<item>
			<title>first item</title>
			<description>first description</description>
		</item>
		<item>
			<title>second item</title>
			<description>second description</description>
		</item>
	</channel>
</rss>`;

const minimalExampleFeed = {
	$: {
		version: '2.0',
	},
	channel: {
		title: { _: 'bla' },
		link: { _: 'example.com' },
		description: { _: 'blabla' },
		item: [
			{
				title: { _: 'first item' },
				description: { _: 'first description' },
			},
			{
				title: { _: 'second item' },
				description: { _: 'second description' },
			},
		],
	},
};


