export const isValidObject = (x: unknown): x is Record<string, unknown> => {
	return x !== null && typeof x === 'object';
};
