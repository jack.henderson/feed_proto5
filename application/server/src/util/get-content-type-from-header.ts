export const getContentTypeFromHeader = (header: string): string => {
	return header.split(';')[0].trim();
};
