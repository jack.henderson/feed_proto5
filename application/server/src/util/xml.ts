import { parseStringPromise } from 'xml2js';
import { normalize } from 'xml2js/lib/processors';
import { isValidObject } from './object';


export namespace XML {

	export type Document = Record<string, unknown>;

	export type CharContent = { _: string };
	export const hasCharContent = (x: unknown): x is CharContent & Record<string, unknown> => {
		if(!isValidObject(x)) {
			return false;
		}

		return typeof x._ === 'string';
	};

	export const charContent = (doc: Record<string, unknown>, tag: string): string => {
		const value = doc[tag];
		if(!XML.hasCharContent(value)) {
			return '';
		}

		return value._;
	};

	/**
	 * Try to parse the passed string as XML into an object.
	 * @param source string to try to parse as XML.
	 * @return an object parsed from XML, **or null** if `source` is not valid XML.
	 */
	export const parse = async (source: string): Promise<XML.Document | null> => {
		try {
			return await parseStringPromise(source, {
				normalizeTags: true,
				explicitCharkey: true,
				explicitArray: false,
				attrNameProcessors: [ normalize ],
			});
		} catch(err) {
			return null;
		}
	};
}
