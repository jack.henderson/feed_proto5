import { makeServer, startServer, makeServerConfigFromEnvironment } from './init';


const config = makeServerConfigFromEnvironment();
const server = makeServer(config);
startServer(server, config);
