import { FastifyLoggerOptions } from 'fastify';

export const SERVER_CONFIG_PORT_ENV_VAR = 'FEED_SERVER_PORT';
export const SERVER_CONFIG_HOST_ENV_VAR = 'FEED_SERVER_HOST';
export const SERVER_CONFIG_LOG_ENV_VAR  = 'FEED_SERVER_LOGGING';

export const LOGGER_STDOUT = 'stdout';

export type ServerConfig = {
	port: number;
	host: string;
	logging: string;
};

const envVarToConfigKey: {[key: string]: string} = {
	[SERVER_CONFIG_PORT_ENV_VAR]: 'port',
	[SERVER_CONFIG_HOST_ENV_VAR]: 'host',
	[SERVER_CONFIG_LOG_ENV_VAR]:  'logging',
};



export const defaultServerConfig: ServerConfig = {
	port: 8080,
	host: 'localhost',
	logging: 'stdout',
};

export const makeServerConfig = (environment: {[key: string]: string}): ServerConfig => {
	const environmentConfig = Object.keys(environment)
		.filter(k => k in envVarToConfigKey)
		.map(k => [ k, environment[k] ])
		.map(kv => {
			switch(kv[0]) {
				case SERVER_CONFIG_PORT_ENV_VAR: {
					const portNum = parseInt(kv[1], 10);
					if(isNaN(portNum)) {
						throw new Error(`Invalid FEED_SERVER_PORT: ${kv[1]}`);
					}
					return [ kv[0], portNum];
				}
				default:
					return kv;
			}
		})
		.reduce<Partial<ServerConfig>>((config, keyValuePair) => ({
			...config,
			[envVarToConfigKey[keyValuePair[0]]]: keyValuePair[1],
		}), {});

	return {
		...defaultServerConfig,
		...environmentConfig,
	};
};

export const makeLoggerConfig = (config: ServerConfig): FastifyLoggerOptions => {
	if(config.logging === 'stdout') {
		return {
			level: 'info',
		};
	}

	return {
		file: config.logging,
	} as FastifyLoggerOptions; // Typings appear to be missing 'file'...
};

export const makeServerConfigFromEnvironment = (): ServerConfig => {
	return makeServerConfig(process.env as {[key: string]: string});
};
