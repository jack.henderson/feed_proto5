import { ServerConfig, makeLoggerConfig } from './config';
import fastify, { FastifyInstance } from 'fastify';


export const makeServer = (config: ServerConfig): FastifyInstance => {
	const server = fastify({
		logger: makeLoggerConfig(config),
	});

	return server;
};

export const startServer = (server: FastifyInstance, config: ServerConfig): void => {
	(async () => {
		const addr = await server.listen(config.port, config.host);
		console.log(`*** feed server listening @ ${addr} ***`);
	})();
};
