import { Channel, Item } from '../common';


type ProvideChannelResult_NoChannelFound = {
	status: 'no-channel-found';
};

type ProvideChannelResult_Error = {
	status: 'error';
	errors: string[];
};

type ProvideChannelResult_OK = {
	status: 'ok';
	channel: Channel;
	items: Item[];
};


export type ChannelProviderResult =
		ProvideChannelResult_NoChannelFound
	| ProvideChannelResult_Error
	| ProvideChannelResult_OK
;

export namespace ProvideChannelResult {
	const noChannelFoundResult: ChannelProviderResult = {
		status: 'no-channel-found',
	};
	export const noChannelFound = (): ChannelProviderResult => {
		return noChannelFoundResult;
	};

	export const error = (...errors: string[]): ChannelProviderResult => {
		return {
			status: 'error',
			errors,
		};
	};

	export const ok = (channel: Channel, items: Item[]): ChannelProviderResult => {
		return {
			status: 'ok',
			channel,
			items,
		};
	};

	export const isOk = (result: ChannelProviderResult): result is ProvideChannelResult_OK => result.status === 'ok';
}
