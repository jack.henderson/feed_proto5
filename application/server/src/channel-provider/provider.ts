import { ChannelProviderResult } from './result';
import { ChannelProviderSource } from './source';


export type ChannelProvider = (src: ChannelProviderSource) => Promise<ChannelProviderResult>;

