export type ChannelProviderSource = {
	contentType: string;
	content: string;
};
