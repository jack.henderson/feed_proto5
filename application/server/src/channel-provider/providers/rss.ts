import { ChannelProvider } from '../provider';
import { ProvideChannelResult as ChannelProviderResult } from '../result';
import { XML, isValidObject } from '../../util';
import { Channel, Item } from '../../common';


export namespace RSS {
 export const provider: ChannelProvider = async (src) => {
		if(!acceptContentType(src.contentType)) {
			return ChannelProviderResult.noChannelFound();
		}

		const feed = await parseRSS(src.content);
		if(!feed) {
			return ChannelProviderResult.noChannelFound();
		}

		return ChannelProviderResult.ok(
			makeChannel(feed),
			makeItems(feed),
		);
	};

	/**
	 * Check if the passed string represents something to try and parse as an RSS feed.
	 * @param contentType the ContentType to test
	 */
	export const acceptContentType = (contentType: string): boolean => {
		switch(contentType) {
			case 'application/rss+xml':
			case 'application/rdf+xml':
			case 'application/atom+xml':
			case 'application/xml':
			case 'text/xml':
				return true;
			default:
				return false;
		}
	};

	/**
	 * Defines an RSS feed as parsed by XML.parse().
	 */
	type Feed = {
		$?: { [key: string]: string; };
		channel: {
			title: XML.CharContent;
			link: XML.CharContent;
			description: XML.CharContent;
			item: Record<string, unknown>[];
			[key: string]: XML.CharContent | Record<string, unknown> | Record<string, unknown>[];
		};
	};

	/**
	 * Required fields in a `<channel>` element, as defined by the RSS 2.0 spec.
	 */
	const requiredChannelFields = [
		'title', 'description', 'link',
	];

	/**
	 * Type-guard for a `Feed` object.
	 * @param x object to check
	 * @returns true if `x` is a valid `Feed` object.
	 */
	const isFeed = (x: unknown): x is Feed => {
		if(!isValidObject(x)) {
			return false;
		}

		const { channel } = x;
		if(!isValidObject(channel)) {
			return false;
		}

		const channelHasRequiredFields = requiredChannelFields
			.map(f => channel[f])
			.every(XML.hasCharContent);
		if(!channelHasRequiredFields) {
			return false;
		}

		if(!Array.isArray(channel.item)) {
			return false;
		}

		return true;
	};

	/**
	 * Try to parse the passed string into an RSS Feed.
	 * @param src string to parse
	 * @return the parsed Feed, **or null** if no feed could be parsed.
	 */
	export const parseRSS = async (src: string): Promise<Feed | null> => {
		const doc = await XML.parse(src);
		if(!isValidObject(doc)) {
			return null;
		}

		const { rss } = doc;
		if(!isFeed(rss)) {
			return null;
		}

		return rss;
	};

	/**
	 * Extract the `Channel` information from a `Feed` object.
	 * @param rss a Feed object, as parsed by `RSS.parseRSS()`.
	 */
	export const makeChannel = (rss: Feed): Channel => {
		return {
			title: XML.charContent(rss.channel, 'title'),
			description: XML.charContent(rss.channel, 'description'),
			link: XML.charContent(rss.channel, 'link'),
			type: 'rss',
		};
	};

	/**
	 * Extract the `Item` information from a `Feed` object.
	 * @param rss a Feed object, as parsed by `RSS.parseRSS()`.
	 */
	export const makeItems = (rss: Feed): Item[] => {
		return rss.channel.item.map<Item>(i => ({
			title: XML.charContent(i, 'title'),
			description: XML.charContent(i, 'description'),
		}));
	};
}
