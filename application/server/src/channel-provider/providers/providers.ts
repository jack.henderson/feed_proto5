import { ChannelProvider } from '../provider';
import { ChannelType } from '../../common';
import { RSS } from './rss';

export const channelProviders = new Map<ChannelType, ChannelProvider>();
channelProviders.set('rss', RSS.provider);
